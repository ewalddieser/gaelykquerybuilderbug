import groovyx.gaelyk.datastore.Entity
import groovyx.gaelyk.datastore.Key


@Entity(unindexed = false)
class Task {
    @Key
    String taskId
    String userId
}