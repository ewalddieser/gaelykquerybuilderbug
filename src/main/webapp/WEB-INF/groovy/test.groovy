import com.google.appengine.api.datastore.DatastoreService
import com.google.appengine.api.datastore.DatastoreServiceFactory
import groovyx.gaelyk.query.QueryBuilder


DatastoreService datastore = DatastoreServiceFactory.datastoreService
QueryBuilder queryBuilder
queryBuilder = datastore.build {
    from 'Task' as Task
    where userId == 'test@test.com'
}